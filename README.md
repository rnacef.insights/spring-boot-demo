# spring-boot-demo

This repository has been created as an example/demo of how we recommend to use spring boot. This example is a web service that allows you to create, read, update and delete users as well as some additional queries.

## Pre-requisites
Ensure you have the following installed on your machine to run this example:
* Java 11
* Docker

## Getting Started
Clone the repository and ensure you are in the repository root directory.

### Database
There is docker-compose file containing two services, one for postgresql and one for adminer. Run the following command to get this up and running:
`docker-compose up`

### Application
We can run the application with gradle. There is a gradle wrapper included in the repository. Run the following command to start the application:
* On linux: `./gradlew bootRun`
* On windows: `gradlew.bat bootRun`
