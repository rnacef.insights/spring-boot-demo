package com.insights.demo.controller;

import com.insights.demo.api.CreateUserRequest;
import com.insights.demo.repository.entity.User;
import com.insights.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public List<User> getUsers() {
        return userService.getUsers();
    }

    @PostMapping
    public User createUser(@RequestBody CreateUserRequest userRequest) {
        return userService.createUser(userRequest);
    }

    @GetMapping("/{userId}")
    public User getUser(@PathVariable Long userId) {
        return userService.getUser(userId);
    }

    @PutMapping("/{userId}")
    public User updateUser(@PathVariable Long userId, @RequestBody CreateUserRequest userRequest) {
        return userService.updateUser(userId, userRequest);
    }

    @DeleteMapping("/{userId}")
    @ResponseStatus(value = HttpStatus.OK)
    public void deleteUser(@PathVariable Long userId) {
        userService.deleteUser(userId);
    }

    // Three ways of doing the same query

    @GetMapping("/method")
    public List<User> getUsersByFirstNames(@RequestParam List<String> names) {
        return userService.getUsersByFirstNames(names);
    }

    @GetMapping("/jpql")
    public List<User> getUsersByFirstNamesJpql(@RequestParam List<String> names) {
        return userService.getUsersByFirstNamesJpql(names);
    }

    @GetMapping("native")
    public List<User> getUsersByFirstNamesNative(@RequestParam List<String> names) {
        return userService.getUsersByFirstNamesNative(names);
    }

}
