package com.insights.demo.repository;

import com.insights.demo.repository.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    @Override
    List<User> findAll();

    // Three ways of doing the same query

    // Spring magic with method name
    List<User> findByFirstNameIn(List<String> names);

    // JPQL Query
    @Query("SELECT users FROM User users WHERE users.firstName IN (:names)")
    List<User> findByFirstNamesJpql(@Param("names") List<String> names);

    // Native Query
    @Query(nativeQuery = true, value = "SELECT * FROM demo_users as users WHERE users.first_name IN (:names)")
    List<User> findByFirstNamesNative(@Param("names") List<String> names);

}
