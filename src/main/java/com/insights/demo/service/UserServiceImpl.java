package com.insights.demo.service;

import com.insights.demo.api.CreateUserRequest;
import com.insights.demo.repository.UserRepository;
import com.insights.demo.repository.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getUsers() {
        return userRepository.findAll();
    }

    @Override
    public User createUser(CreateUserRequest userRequest) {
        return userRepository.save(new User(userRequest.getFirstName(), userRequest.getLastName()));
    }

    @Override
    public User getUser(Long userId) {
        return userRepository.findById(userId).orElseThrow();
    }

    @Override
    public User updateUser(Long userId, CreateUserRequest userRequest) {
        return userRepository.save(new User(userId, userRequest.getFirstName(), userRequest.getLastName()));
    }

    @Override
    public void deleteUser(Long userId) {
        userRepository.deleteById(userId);
    }

    // Three ways of doing the same query

    @Override
    public List<User> getUsersByFirstNames(List<String> names) {
        return userRepository.findByFirstNameIn(names);
    }

    @Override
    public List<User> getUsersByFirstNamesJpql(List<String> names) {
        return userRepository.findByFirstNamesJpql(names);
    }

    @Override
    public List<User> getUsersByFirstNamesNative(List<String> names) {
        return userRepository.findByFirstNamesNative(names);
    }

}
