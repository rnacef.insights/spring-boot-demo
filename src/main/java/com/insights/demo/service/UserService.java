package com.insights.demo.service;

import com.insights.demo.api.CreateUserRequest;
import com.insights.demo.repository.entity.User;

import java.util.List;

public interface UserService {
    List<User> getUsers();

    User createUser(CreateUserRequest userRequest);

    User getUser(Long userId);

    User updateUser(Long userId, CreateUserRequest userRequest);

    void deleteUser(Long userId);

    // Three ways of doing the same query

    List<User> getUsersByFirstNames(List<String> names);

    List<User> getUsersByFirstNamesJpql(List<String> names);

    List<User> getUsersByFirstNamesNative(List<String> names);
}
